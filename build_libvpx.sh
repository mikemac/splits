#!/bin/sh

cd ~/src

wget http://storage.googleapis.com/downloads.webmproject.org/releases/webm/libvpx-1.4.0.tar.bz2
tar xf libvpx-1.4.0.tar.bz2
cd libvpx-1.4.0
./configure --enable-shared --enable-pic
make -j2
sudo make install

cd $HOME
