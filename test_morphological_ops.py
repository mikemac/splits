import sys

import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster as skcl
import cv2

def gk(n):
    return np.ones((n, n), np.uint8)
    return cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (n,n))

def main(args):

    # img = np.rot90(np.rot90(np.rot90(np.load(args[0]))))
    img = np.load(args[0])
    assert set(img.ravel()) == set([0,255]), 'unexpected values'
    xmax, ymax = img.shape

    for ks in (3, 5, 7, 9, 11, 13):
        print ks

        erosion = cv2.erode(img, gk(ks), iterations = 2)
        dilation = cv2.dilate(erosion, gk(ks + 2), iterations = 5)
        #opening = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
        cv2.imshow('smurf', dilation)

        k = cv2.waitKey(0)

    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv[1:])
