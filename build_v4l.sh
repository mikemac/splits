#!/bin/sh

cd ~/src

wget http://www.linuxtv.org/downloads/v4l-utils/v4l-utils-1.8.0.tar.bz2
tar xvf v4l-utils-1.8.0.tar.bz2
cd v4l-utils-1.8.0
./bootstrap.sh
./configure --enable-shared
make -j2
sudo make install

cd $HOME

