import sys
import shutil

W = 4

def main(args):

    for fn in args:
        parts = fn.split('.')
        pre, suf = parts[0].split('_')
        tag = '%s_%0' + str(W) + 'd'
        parts[0] = tag % (pre, int(suf))
        fn_new = '.'.join(parts)
        print fn, fn_new
        shutil.move(fn, fn_new)

if __name__ == '__main__':
    main(sys.argv[1:])
