import sys
import itertools as itr

import numpy as np
import cv2

args = sys.argv[1:]
vfile = args[0]
keep = int(args[1]) if len(args) > 1 else -1

cap = cv2.VideoCapture(vfile)

fgbg = cv2.createBackgroundSubtractorKNN(detectShadows=False)
#fgbg = cv2.createBackgroundSubtractorMOG2(detectShadows=False)
#fgbg = cv2.BackgroundSubtractorMOG()
#fgbg = cv2.BackgroundSubtractorMOG2()

# vout = cv2.VideoWriter('video.mp4',
#                        cv2.VideoWriter_fourcc(*'AVC1'),
#                        29.97,
#                        (1280,720),
#                        isColor=True)

vout = cv2.VideoWriter('video.mp4',
                       cv2.VideoWriter_fourcc(*'AVC1'),
                       29.97,
                       (1280,720),
                       isColor=False)

for framen in itr.count(1):
    ret, frame = cap.read()
    if not ret:
        break

    print >> sys.stderr, 'Frame [%d] {ret=%s}' % (framen, ret)
    # vout.write(frame)
    #print type(frame), frame.shape, frame[:3, :3, :]

    # do bg sub and show
    fgmask = fgbg.apply(frame)
    vout.write(fgmask)
    # cv2.imshow('frame',fgmask)
    np.save('frame_%00d.npy' % framen, fgmask)
    cv2.imwrite('frame_%00d.png' % framen, fgmask)
    cv2.imwrite('image_%00d.png' % framen, frame)

    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

    if keep > 0:
        if framen >= keep:
            break


cap.release()
vout.release()
cv2.destroyAllWindows()
