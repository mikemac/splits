#!/bin/sh

cd ~/src

wget http://ffmpeg.org/releases/ffmpeg-2.8.tar.bz2
tar xvf ffmpeg-2.8.tar.bz2
cd ffmpeg-2.8
./configure --enable-gpl --enable-libfaac --enable-libmp3lame --enable-libopencore-amrnb --enable-libopencore-amrwb --enable-libtheora --enable-libvorbis --enable-libx264 --enable-libxvid --enable-nonfree --enable-postproc --enable-version3 --enable-x11grab --enable-shared --enable-libvpx --enable-pic
make
sudo make install

cd $HOME
