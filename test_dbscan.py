import sys
import os

import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster as skcl

min_size = 500
min_area = 5000

COLORS = ('green', 'blue', 'orange', 'pink', 'black')

def bbox(xp, yp):
    return (xp.min(), xp.max(),
            yp.min(), yp.max())

def hist(v):
    out = {}
    for e in v:
        if e in out:
            out[e] += 1
        else:
            out[e] = 1
    return out

def main(args):

    # check if we've already processed
    infile = args[0]
    outfile = infile.replace('.npy', '.clust.png')
    if os.path.exists(outfile):
        print >> sys.stderr, 'Skipping [%s], already processed.' % infile
        return

    image_data = np.rot90(np.rot90(np.rot90(np.load(infile))))
    assert not (set(image_data.ravel()) - set([0,255])), 'unexpected values'
    image_hist = hist(image_data.ravel())

    white_ratio = float(image_hist.get(255, 0)) / sum(image_hist.values())
    if white_ratio > 0.25:
        # image has tons of white, draw empty image and punt
        plt.xlim(0, 1280)
        plt.ylim(0, 720)
        plt.savefig(outfile)
        return

    xmax, ymax = image_data.shape

    data = np.vstack(np.where(image_data == 255)).transpose()
    #np.random.shuffle(data)

    result = skcl.dbscan(data, eps=7, min_samples=3)
    centroids, labels = result

    color_idx = 0
    # plot the points for each label
    for lbl in set(labels):

        x = data[labels == lbl, 0]
        y = data[labels == lbl, 1]
        cluster_size = len(x)
        cxmin, cxmax, cymin, cymax = bbox(x, y)
        cluster_area = (cxmax - cxmin) * (cymax - cymin)


        if lbl < 0 or cluster_size < min_size or cluster_area < min_area:
            plt.plot(x, y, color='0.5', marker='.', linestyle='None')
            continue

        print args[0], lbl, cxmin, cxmax, cymin, cymax, cluster_size, cluster_area
        plt.plot(x, y,
                 color=COLORS[color_idx],
                 alpha=0.25,
                 marker='o',
                 linestyle='None',
                 markeredgecolor='None')
        color_idx = (color_idx + 1) % len(COLORS)

    plt.xlim(0, 1280)
    plt.ylim(0, 720)
    plt.savefig(outfile)

if __name__ == '__main__':
    main(sys.argv[1:])
