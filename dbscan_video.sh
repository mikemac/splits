#!/bin/sh

if [ ! -e coords ]
then
    touch coords
fi

for ff in $(ls frame*.npy)
do
    echo $ff
    python splits/test_dbscan.py $ff >> coords
done
