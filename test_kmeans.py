import sys

import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster as skcl

meanshift = True

def main(args):

    image_data = np.rot90(np.rot90(np.rot90(np.load(args[0]))))
    assert set(image_data.ravel()) == set([0,255]), 'unexpected values'

    xmax, ymax = image_data.shape

    data = np.vstack(np.where(image_data == 255)).transpose()
    np.random.shuffle(data)

    km_result = skcl.k_means(data, 7)
    centroids, labels = km_result[:2]

    # -- run mean shift with the k-means centers as seeds
    if meanshift:
        bw = skcl.estimate_bandwidth(data, n_samples=1000)
        print >> sys.stderr, "bandwidth:", bw
        ms_result = skcl.mean_shift(data,
                                    bandwidth=bw,
                                    seeds=centroids,
                                    cluster_all=False,
                                )
        centroids, labels = ms_result[:2]

    # plot the points for each label
    for lbl in set(labels):
        if lbl < 0:
            continue
        x = data[labels == lbl, 0]
        y = data[labels == lbl, 1]
        plt.plot(x, y, 'o')
        print lbl
        print len(x)


    plt.show()

if __name__ == '__main__':
    main(sys.argv[1:])
