#!/bin/sh

cd ~/src

wget https://github.com/Itseez/opencv/archive/3.0.0.zip
unzip 3.0.0.zip

cd opencv-3.0.0

mkdir -p build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE ..
make -j2
sudo make install

cd $HOME
