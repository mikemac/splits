#!/bin/sh

sudo apt-get update -y -q
sudo apt-get upgrade -y -q
sudo apt-get dist-upgrade -y -q

sudo apt-get -y -q install \
     \
     byobu \
     emacs \
     eog \
     git \
     htop \
     vim \
     zsh \
     \
     ipython \
     ipython-notebook \
     ipython-qtconsole \
     python-matplotlib \
     python-numpy \
     python-pandas \
     python-scipy \
     python-sklearn \
     python-statsmodels \
     \
     ipython3 \
     ipython3-notebook \
     ipython3-qtconsole \
     python3-matplotlib \
     python3-numpy \
     python3-pandas \
     python3-scipy

sudo apt-get autoremove -y -q
