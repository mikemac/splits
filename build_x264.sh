#!/bin/sh

cd ~/src

wget ftp://ftp.videolan.org/pub/videolan/x264/snapshots/x264-snapshot-20150920-2245-stable.tar.bz2
tar xvf x264-snapshot-20150920-2245-stable.tar.bz2
cd x264-snapshot-20150920-2245-stable
./configure --enable-shared --enable-pic
make -j2
sudo make install

cd $HOME
